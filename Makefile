.PHONY: all clean fmt test doc doc-browse doc-commit-to-pages
all:
	dune build

clean:
	dune clean

fmt:
	dune build @fmt --auto-promote

test:
	dune runtest

doc:
	dune build @doc

DOC_PATH := _build/default/_doc/_html

doc-browse: doc
	open ${DOC_PATH}/index.html

doc-commit-to-pages: doc
# compute the current commit hash and branch name
	@git rev-parse --short HEAD > /tmp/commit
	@git rev-parse --abbrev-ref HEAD > /tmp/branch
	cp .gitlab-ci.yml /tmp

# move to the 'pages' branch and commit the documentation there
	git checkout pages
	mkdir -p docs
	@git rm --ignore-unmatch -r docs > /dev/null
	rm -rf docs
	cp -r ${DOC_PATH} docs
	@git add docs
# we also update the 'pages' CI script to follow original-branch changes
	cp /tmp/.gitlab-ci.yml .
	git add .gitlab-ci.yml
	@git commit -m "documentation for $$(cat /tmp/branch) ($$(cat /tmp/commit))" \
	&& git checkout $$(cat /tmp/branch) \
	|| git checkout $$(cat /tmp/branch)

	@echo
	@echo "The documentation was committed to the 'pages' branch."
	@echo "You can now push it with"
	@echo "    git push origin pages"
