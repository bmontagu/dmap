(**************************************************************************)
(*                                                                        *)
(*   Authors:                                                             *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*   Benoît Montagu, projet Celtique, INRIA Rennes Bretagne Atlantique    *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** The type of comparisons: [('a, 'b) cmp] denotes comparisons
   between values of types ['a] and ['b]. The types ['a] and ['b]
   might be different. [Lt] denotes "lesser than", [Eq] denotes
   "equal", and [Gt] denotes "greater than". When the [Eq] constructor
   is used, we learn that ['a] and ['b] are the same. *)
type (_, _) cmp = Lt : ('a, 'b) cmp | Eq : ('a, 'a) cmp | Gt : ('a, 'b) cmp

(** The signature of dependently-ordered types. This is the input
   signature of the functor {!Make}. *)
module type DORDERED = sig
  type _ t
  (** The type of the map keys, indexed with the type of the value for
      such keys. *)

  val compare : 'a t -> 'b t -> ('a, 'b) cmp
end

(** Output signature of the functor {!MakeWithValue}. *)
module type S_WITH_VALUE = sig
  type 'a key
  (** The type of the map keys, indexed with the type of the value for
      such keys. *)

  type 'a value

  type t
  (** The type of maps from type ['a key] to type ['a]. *)

  val empty : t
  (** The empty map. *)

  val is_empty : t -> bool
  (** Test whether a map is empty or not. *)

  val mem : 'a key -> t -> bool
  (** [mem x m] returns [true] if [m] contains a binding for [x],
       and [false] otherwise. *)

  val add : 'a key -> 'a value -> t -> t
  (** [add key data m] returns a map containing the same bindings as
   [m], plus a binding of [key] to [data]. If [key] was already bound
   in [m] to a value that is physically equal to [data], [m] is
   returned unchanged (the result of the function is then physically
   equal to [m]). Otherwise, the previous binding of [key] in [m]
   disappears. *)

  val update : 'a key -> ('a value option -> 'a value option) -> t -> t
  (** [update key f m] returns a map containing the same bindings as
   [m], except for the binding of [key]. Depending on the value of [y]
   where [y] is [f (find_opt key m)], the binding of [key] is added,
   removed or updated. If [y] is [None], the binding is removed if it
   exists; otherwise, if [y] is [Some z] then [key] is associated to
   [z] in the resulting map. If [key] was already bound in [m] to a
   value that is physically equal to [z], [m] is returned unchanged
   (the result of the function is then physically equal to [m]). *)

  val singleton : 'a key -> 'a value -> t
  (** [singleton x y] returns the one-element map that contains a
   binding [y] for [x]. *)

  val remove : 'a key -> t -> t
  (** [remove x m] returns a map containing the same bindings as [m],
   except for [x] which is unbound in the returned map. If [x] was not
   in [m], [m] is returned unchanged (the result of the function is
   then physically equal to [m]). *)

  type poly_merge = {
    merge_fun :
      'a. 'a key -> 'a value option -> 'a value option -> 'a value option;
  }
  [@@unboxed]

  val merge : poly_merge -> t -> t -> t
  (** [merge f m1 m2] computes a map whose keys are a subset of the keys
   of [m1] and of [m2]. The presence of each such binding, and the
   corresponding value, is determined with the function [f]. In terms
   of the [find_opt] operation, we have [find_opt x (merge f m1 m2) =
   f x (find_opt x m1) (find_opt x m2)] for any key [x], provided that
   [f x None None = None].

   The function [f] must be put inside a record of type {!poly_merge}
   to ensure it is polymorphic. It is indeed used in a polymorphic
   manner by the implementation of {!merge}. *)

  type poly_union = {
    union_fun : 'a. 'a key -> 'a value -> 'a value -> 'a value option;
  }
  [@@unboxed]

  val union : poly_union -> t -> t -> t
  (** [union f m1 m2] computes a map whose keys are a subset of the keys
        of [m1] and of [m2].  When the same binding is defined in both
        arguments, the function [f] is used to combine them.
        This is a special case of [merge]: [union f m1 m2] is equivalent
        to [merge f' m1 m2], where
        - [f' _key None None = None]
        - [f' _key (Some v) None = Some v]
        - [f' _key None (Some v) = Some v]
        - [f' key (Some v1) (Some v2) = f key v1 v2]

   The function [f] must be put inside a record of type {!poly_union}
   to ensure it is polymorphic. It is indeed used in a polymorphic
   manner by the implementation of {!union}. *)

  type 'b poly0 = { fun0 : 'a. 'a key -> 'b } [@@unboxed]
  (** ['b poly1] is the type of functions that take a key of type ['a key]
   and that return an element of type ['b].
   They are polymorphic in ['a]. *)

  type 'b poly1 = { fun1 : 'a. 'a key -> 'a value -> 'b } [@@unboxed]
  (** ['b poly1] is the type of functions that take a key of type ['a key]
   and one value of type ['a value], and that return an element of type ['b].
   They are polymorphic in ['a]. *)

  type 'b poly2 = { fun2 : 'a. 'a key -> 'a value -> 'a value -> 'b }
  [@@unboxed]
  (** ['b poly2] is the type of functions that take a key of type ['a key]
   and two values of type ['a value], and that return an element of type ['b].
   They are polymorphic in ['a]. *)

  val compare : int poly2 -> t -> t -> int
  (** Total ordering between maps. The first argument is a total
   ordering used to compare data associated with equal keys in the two
   maps. *)

  val equal : bool poly2 -> t -> t -> bool
  (** [equal cmp m1 m2] tests whether the maps [m1] and [m2] are equal,
   that is, contain equal keys and associate them with equal data.
   [cmp] is the equality predicate used to compare the data associated
   with the keys. *)

  val iter : unit poly1 -> t -> unit
  (** [iter f m] applies [f] to all bindings in map [m]. [f] receives
   the key as first argument, and the associated value as second
   argument. The bindings are passed to [f] in increasing order with
   respect to the ordering over the type of the keys. *)

  val fold : ('b -> 'b) poly1 -> t -> 'b -> 'b
  (** [fold f m init] computes [(f kN dN ... (f k1 d1 init)...)], where
   [k1 ... kN] are the keys of all bindings in [m] (in increasing
   order), and [d1 ... dN] are the associated data. *)

  val for_all : bool poly1 -> t -> bool
  (** [for_all f m] checks if all the bindings of the map satisfy the
   predicate [f]. *)

  val exists : bool poly1 -> t -> bool
  (** [exists f m] checks if at least one binding of the map satisfies
   the predicate [f]. *)

  val filter : bool poly1 -> t -> t
  (** [filter f m] returns the map with all the bindings in [m] that
   satisfy predicate [p]. If every binding in [m] satisfies [f], [m]
   is returned unchanged (the result of the function is then
   physically equal to [m]) *)

  type poly_filter_map = {
    filter_map_fun : 'a. 'a key -> 'a value -> 'a value option;
  }
  [@@unboxed]

  val filter_map : poly_filter_map -> t -> t
  (** [filter_map f m] applies the function [f] to every binding of
        [m], and builds a map from the results. For each binding
        [(k, v)] in the input map:
        - if [f k v] is [None] then [k] is not in the result,
        - if [f k v] is [Some v'] then the binding [(k, v')]
          is in the output map.

        For example, the following function on maps whose values are lists
        {[
        filter_map
          (fun _k li -> match li with [] -> None | _::tl -> Some tl)
          m
        ]}
        drops all bindings of [m] whose value is an empty list, and pops
        the first element of each value that is non-empty.
     *)

  val partition : bool poly1 -> t -> t * t
  (** [partition f m] returns a pair of maps [(m1, m2)], where [m1]
   contains all the bindings of [m] that satisfy the predicate [f],
   and [m2] is the map with all the bindings of [m] that do not
   satisfy [f]. *)

  val cardinal : t -> int
  (** Return the number of bindings of a map. *)

  (** The type of bindings: a key and its associated value *)
  type binding = Binding : 'a key * 'a value -> binding

  val bindings : t -> binding list
  (** Return the list of all bindings of the given map. The returned
   list is sorted in increasing order of keys with respect to the
   ordering [Ord.compare], where [Ord] is the argument given to
   {!Make}. *)

  val min_binding : t -> binding
  (** Return the binding with the smallest key in a given map (with
   respect to the [Ord.compare] ordering), or raise [Not_found] if the
   map is empty. *)

  val min_binding_opt : t -> binding option
  (** Return the binding with the smallest key in the given map (with
   respect to the [Ord.compare] ordering), or [None] if the map is
   empty. *)

  val max_binding : t -> binding
  (** Same as {!S.min_binding}, but returns the binding with the largest
   key in the given map. *)

  val max_binding_opt : t -> binding option
  (** Same as {!S.min_binding_opt}, but returns the binding with the
   largest key in the given map. *)

  val choose : t -> binding
  (** Return one binding of the given map, or raise [Not_found] if the
   map is empty. Which binding is chosen is unspecified, but equal
   bindings will be chosen for equal maps. *)

  val choose_opt : t -> binding option
  (** Return one binding of the given map, or [None] if the map is
   empty. Which binding is chosen is unspecified, but equal bindings
   will be chosen for equal maps. *)

  val split : 'a key -> t -> t * 'a value option * t
  (** [split x m] returns a triple [(l, data, r)], where:
        - [l] is the map with all the bindings of [m] whose key
        is strictly less than [x];
        - [r] is the map with all the bindings of [m] whose key
        is strictly greater than [x];
        - [data] is [None] if [m] contains no binding for [x],
          or [Some v] if [m] binds [v] to [x].
     *)

  val find : 'a key -> t -> 'a value
  (** [find x m] returns the current value of [x] in [m], or raises
   [Not_found] if no binding for [x] exists. *)

  val find_opt : 'a key -> t -> 'a value option
  (** [find_opt x m] returns [Some v] if the current value of [x] in [m]
   is [v], or [None] if no binding for [x] exists. *)

  val find_first : bool poly0 -> t -> binding
  (** [find_first f m], where [f] is a monotonically increasing
   function, returns the binding of [m] with the lowest key [k] such
   that [f k], or raises [Not_found] if no such key exists.

   For example, [find_first (fun k -> Ord.compare k x >= 0) m] will
   return the first binding [k, v] of [m] where [Ord.compare k x >= 0]
   (intuitively: [k >= x]), or raise [Not_found] if [x] is greater
   than any element of [m]. *)

  val find_first_opt : bool poly0 -> t -> binding option
  (** [find_first_opt f m], where [f] is a monotonically increasing
   function, returns an option containing the binding of [m] with the
   lowest key [k] such that [f k], or [None] if no such key exists. *)

  val find_last : bool poly0 -> t -> binding
  (** [find_last f m], where [f] is a monotonically decreasing function,
   returns the binding of [m] with the highest key [k] such that [f k],
   or raises [Not_found] if no such key exists. *)

  val find_last_opt : bool poly0 -> t -> binding option
  (** [find_last_opt f m], where [f] is a monotonically decreasing
   function, returns an option containing the binding of [m] with the
   highest key [k] such that [f k], or [None] if no such key exists.
   *)

  type poly_mapi = { mapi_fun : 'a. 'a key -> 'a value -> 'a value } [@@unboxed]

  val mapi : poly_mapi -> t -> t
  (** [mapo f m] returns a map with same domain as [m], where the value
   [a] associated to a key [k] in the map [m] has been replaced by the
   result of the application of [f] to [k] and [a]. The bindings are
   passed to [f] in increasing order with respect to the ordering over
   the type of the keys. *)

  (** {1 Maps and Sequences} *)

  val to_seq : t -> binding Seq.t
  (** Iterate on the whole map, in ascending order of keys *)

  val to_rev_seq : t -> binding Seq.t
  (** Iterate on the whole map, in descending order of keys *)

  val to_seq_from : 'a key -> t -> binding Seq.t
  (** [to_seq_from k m] iterates on a subset of the bindings of [m], in
   ascending order of keys, from key [k] or above. *)

  val add_seq : binding Seq.t -> t -> t
  (** Add the given bindings to the map, in order. *)

  val of_seq : binding Seq.t -> t
  (** Build a map from the given bindings *)
end

module type S = S_WITH_VALUE with type 'a value := 'a
(** Output signature of the functor {!Make}. *)

(** Signature for types with 1 parameter. *)
module type TYPE1 = sig
  type 'a t
end

module type INTF = sig
  (** The type of comparisons: [('a, 'b) cmp] denotes comparisons
   between values of types ['a] and ['b]. The types ['a] and ['b]
   might be different. [Lt] denotes "lesser than", [Eq] denotes
   "equal", and [Gt] denotes "greater than". When the [Eq] constructor
   is used, we learn that ['a] and ['b] are the same. *)
  type ('a, 'b) cmp =
    | Lt : ('a, 'b) cmp
    | Eq : ('a, 'a) cmp
    | Gt : ('a, 'b) cmp

  module type DORDERED = DORDERED
  (** The signature of dependently-ordered types. This is the input
   signature of the functor {!Make}. *)

  module type S_WITH_VALUE = S_WITH_VALUE
  (** Output signature of the functor {!MakeWithValue}. *)

  module type S = S
  (** Output signature of the functor {!Make}. *)

  module type TYPE1 = TYPE1
  (** Signature for types with 1 parameter. *)

  (** Functor building an implementation of the dependent map structure
   given a totally ordered type of indexed keys and a type of values. *)
  module MakeWithValue (Ord : DORDERED) (Val : TYPE1) :
    S_WITH_VALUE with type 'a key = 'a Ord.t and type 'a value = 'a Val.t

  (** Functor building an implementation of the dependent map structure
   given a totally ordered type of indexed keys. *)
  module Make (Ord : DORDERED) : S with type 'a key = 'a Ord.t

  (** Functor that converts a [DORDERED] into an ordered type *)
  module ToOrdered (X : DORDERED) : sig
    type t = Hide : 'a X.t -> t

    val compare : t -> t -> int
  end

  (** Functor that creates a non-dependendent map for keys of a [DORDERED] type *)
  module MakeMap (X : DORDERED) :
    Map.S
      with type key = ToOrdered(X).t
       and type 'a t = 'a Map.Make(ToOrdered(X)).t

  (** Functor that creates a set of values of a [DORDERED] type *)
  module MakeSet (X : DORDERED) : Set.S with type t = Set.Make(ToOrdered(X)).t

  (** Functor that extends the indices of a [DORDERED] type, given some
   type-level function *)
  module Extend
      (X : DORDERED) (F : sig
        type !'a t
      end) : sig
    type _ t = Extend : 'a X.t -> 'a F.t t

    val compare : 'a t -> 'b t -> ('a, 'b) cmp
  end

  (** Functor that extends the indices of a [DORDERED] type by adding
   some data to its left-hand side *)
  module ExtendL
      (X : DORDERED) (Y : sig
        type t
      end) : sig
    type _ t = Extend : 'a X.t -> (Y.t * 'a) t

    val compare : 'a t -> 'b t -> ('a, 'b) cmp
  end

  (** Functor that extends the indices of a [DORDERED] type by adding
   some data to its right-hand side *)
  module ExtendR
      (X : DORDERED) (Y : sig
        type t
      end) : sig
    type _ t = Extend : 'a X.t -> ('a * Y.t) t

    val compare : 'a t -> 'b t -> ('a, 'b) cmp
  end
end
