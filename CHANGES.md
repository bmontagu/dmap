0.6
===

- refactoring of the library

0.5
===

- added a new, more generic functor, that is parametrized over a type
  of values, that can depend on the type indices of keys (backward
  compatibility is maintained)
- added a more generic functor that extends the indices of a type with
  arbitrary metadata
- ensure OCaml >= 4.12.0 is used, so that we can use injectivity
  annotations on type variables of abstract types

0.4
===

- added functors to create a classic ordered type, and non-dependent
  maps and sets from a DORDERED type
- added functors to extend the keys of a DORDERED type with additional data

0.3
===

- changed interface of update (it was too general to be used in a useful way)

0.2
===

- cosmetic changes to dune/opam description files

0.1
===

- initial release
